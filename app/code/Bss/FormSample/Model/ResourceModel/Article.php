<?php
declare(strict_types=1);
namespace Bss\FormSample\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class Article extends AbstractDb
{
    /**
     * @inheritDoc
     */
    protected function _construct()
    {
        $this->_init('article', 'article_id');
    }
}
