<?php
declare(strict_types=1);
namespace Bss\FormSample\Model\ResourceModel\Article\Grid;

use Bss\FormSample\Model\ResourceModel\Article;
use Exception;
use Magento\Framework\Api\Search\AggregationInterface;
use Magento\Framework\Api\Search\SearchResultInterface;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\View\Element\UiComponent\DataProvider\Document;

class Collection extends Article\Collection implements SearchResultInterface
{
    /**
     * @var string
     */
    protected $_eventPrefix = 'article_grid_collection';

    /**
     * @var string
     */
    protected $_eventObject = 'grid_collection';

    /**
     * @var AggregationInterface
     */
    protected $aggregations;

    /**
     * @var SearchCriteriaInterface
     */
    protected $searchCriteria;

    /**
     * @inheritDoc
     */
    public function _construct()
    {
        $this->_init(Document::class, Article::class);
        $this->_setIdFieldName($this->getResource()->getIdFieldName());
    }

    /**
     * @return AggregationInterface
     */
    public function getAggregations()
    {
        return $this->aggregations;
    }

    /**
     * @param AggregationInterface $aggregations
     * @return $this
     */
    public function setAggregations($aggregations)
    {
        $this->aggregations = $aggregations;
        return $this;
    }

    /**
     * Get search criteria.
     *
     * @return SearchCriteriaInterface
     */
    public function getSearchCriteria()
    {
        return $this->searchCriteria;
    }

    /**
     * Set search criteria.
     *
     * @param SearchCriteriaInterface $searchCriteria
     * @return $this
     */
    public function setSearchCriteria(SearchCriteriaInterface $searchCriteria = null)
    {
        $this->searchCriteria = $searchCriteria;
        return $this;
    }

    /**
     * Get total count.
     *
     * @return int
     */
    public function getTotalCount()
    {
        return $this->getSize();
    }

    /**
     * Set total count.
     *
     * @param int $totalCount
     * @return $this
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function setTotalCount($totalCount)
    {
        return $this;
    }

    /**
     * @param array|null $items
     * @return $this
     * @throws Exception
     */
    public function setItems(array $items = null)
    {
        if ($items) {
            foreach ($items as $item) {
                $this->addItem($item);
            }
        }
        return $this;
    }
}
