<?php
declare(strict_types=1);
namespace Bss\FormSample\Model;

use Bss\FormSample\Api\Data\ArticleSearchResultsInterface;
use Magento\Framework\Api\SearchResults;

/**
 * Service Data Object with Article search results.
 */
class ArticleSearchResults extends SearchResults implements ArticleSearchResultsInterface
{
}
