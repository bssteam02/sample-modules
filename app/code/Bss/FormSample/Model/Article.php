<?php
declare(strict_types=1);
namespace Bss\FormSample\Model;

use Bss\FormSample\Api\Data\ArticleInterface;
use Magento\Framework\Model\AbstractModel;

class Article extends AbstractModel implements ArticleInterface
{
    /**
     * @inheritDoc
     */
    public function _construct()
    {
        $this->_init(ResourceModel\Article::class);
    }

    /**
     * @inheritDoc
     */
    public function getArticleId()
    {
        return $this->getData(self::ARTICLE_ID);
    }

    /**
     * @inheritDoc
     */
    public function setArticleId($id)
    {
        return $this->setData(self::ARTICLE_ID, $id);
    }

    /**
     * @inheritDoc
     */
    public function getIsActive()
    {
        return $this->getData(self::IS_ACTIVE);
    }

    /**
     * @inheritDoc
     */
    public function setIsActive($status)
    {
        return $this->setData(self::IS_ACTIVE, $status);
    }

    /**
     * @inheritDoc
     */
    public function getTitle()
    {
        return $this->getData(self::TITLE);
    }

    /**
     * @inheritDoc
     */
    public function setTitle($title)
    {
        return $this->setData(self::TITLE, $title);
    }

    /**
     * @inheritDoc
     */
    public function getPreviewImg()
    {
        return $this->getData(self::PREVIEW_IMG);
    }

    /**
     * @inheritDoc
     */
    public function setPreviewImg($image)
    {
        return $this->setData(self::PREVIEW_IMG, $image);
    }

    /**
     * @inheritDoc
     */
    public function getTopic()
    {
        return $this->getData(self::TOPIC);
    }

    /**
     * @inheritDoc
     */
    public function setTopic($topic)
    {
        return $this->setData(self::TOPIC, $topic);
    }

    /**
     * @inheritDoc
     */
    public function getContent()
    {
        return $this->getData(self::CONTENT);
    }

    /**
     * @inheritDoc
     */
    public function setContent($content)
    {
        return $this->setData(self::CONTENT, $content);
    }
}
