<?php
declare(strict_types=1);
namespace Bss\FormSample\Block\Adminhtml\Article\Edit;

use Magento\Backend\Block\Widget\Context;
use Bss\FormSample\Model\ArticleRepository;
use Magento\Framework\Exception\NoSuchEntityException;

class GenericButton
{
    /**
     * @var Context
     */
    protected $context;

    /**
     * @var ArticleRepository
     */
    protected $articleRepository;

    /**
     * @param Context $context
     * @param ArticleRepository $articleRepository
     */
    public function __construct(
        Context $context,
        ArticleRepository $articleRepository
    ) {
        $this->context = $context;
        $this->articleRepository = $articleRepository;
    }

    /**
     * Return article id.
     *
     * @return int|null
     */
    public function getArticleId()
    {
        try {
            return $this->articleRepository->get(
                $this->context->getRequest()->getParam('article_id')
            )->getArticleId();
        } catch (NoSuchEntityException $e) {
            return null;
        }
    }

    /**
     * Generate url by route and parameters
     *
     * @param   string $route
     * @param   array $params
     * @return  string
     */
    public function getUrl($route = '', $params = [])
    {
        return $this->context->getUrlBuilder()->getUrl($route, $params);
    }
}
