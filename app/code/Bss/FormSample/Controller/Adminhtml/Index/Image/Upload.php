<?php
declare(strict_types=1);
namespace Bss\FormSample\Controller\Adminhtml\Index\Image;

use Bss\FormSample\Controller\Adminhtml\AbstractArticle;
use Magento\Framework\App\Action\HttpPostActionInterface;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\ResultFactory;

class Upload extends AbstractArticle implements HttpPostActionInterface
{
    /**
     * @return ResponseInterface
     */
    public function execute()
    {
        try {
            $result = $this->articleRepository->getUploader()
                ->saveFileToTmpDir($this->getRequest()->getParam('param_name'));
            $result['cookie'] = [
                'name' => $this->_getSession()->getName(),
                'value' => $this->_getSession()->getSessionId(),
                'lifetime' => $this->_getSession()->getCookieLifetime(),
                'path' => $this->_getSession()->getCookiePath(),
                'domain' => $this->_getSession()->getCookieDomain(),
            ];
        } catch (\Exception $e) {
            $result = ['error' => $e->getMessage(), 'errorcode' => $e->getCode()];
        }
        return $this->resultFactory->create(ResultFactory::TYPE_JSON)->setData($result);
    }
}
