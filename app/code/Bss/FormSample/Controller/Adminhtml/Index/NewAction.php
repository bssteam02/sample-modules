<?php
declare(strict_types=1);
namespace Bss\FormSample\Controller\Adminhtml\Index;

use Bss\FormSample\Controller\Adminhtml\AbstractArticle;
use Magento\Framework\App\Action\HttpGetActionInterface;
use Magento\Framework\Controller\ResultInterface;

class NewAction extends AbstractArticle implements HttpGetActionInterface
{
    /**
     * @return ResultInterface
     */
    public function execute()
    {
        return $this->getResultPage(__("New Article"));
    }
}
