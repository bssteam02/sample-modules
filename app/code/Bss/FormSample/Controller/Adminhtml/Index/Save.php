<?php
declare(strict_types=1);
namespace Bss\FormSample\Controller\Adminhtml\Index;

use Bss\FormSample\Controller\Adminhtml\AbstractArticle;
use Magento\Framework\App\Action\HttpPostActionInterface;
use Magento\Framework\Controller\ResultInterface;

class Save extends AbstractArticle implements HttpPostActionInterface
{
    /**
     * @return ResultInterface
     */
    public function execute()
    {
        try {
            $params = $this->getRequest()->getParams();
            $id = $this->getRequest()->getParam('article_id');
            $type = $this->getRequest()->getParam('back');
            if ($id) {
                $article = $this->articleRepository->get($id);
            } else {
                unset($params['article_id']);
                $article = $this->articleRepository->getNew();
            }
            $article->setData($params);
            $this->articleRepository->save($article);
            $this->messageManager->addSuccessMessage(__("You saved the article"));
            if ($type !== 'close') {
                return $this->resultFactory
                    ->create($this->resultFactory::TYPE_REDIRECT)
                    ->setPath('*/*/edit', ['article_id' => $article->getArticleId()]);
            }
        } catch (\Exception $e) {
            $this->messageManager->addErrorMessage(__("There was an error saving the article."));
        }
        return $this->resultFactory->create($this->resultFactory::TYPE_REDIRECT)->setPath('*/*');
    }
}
