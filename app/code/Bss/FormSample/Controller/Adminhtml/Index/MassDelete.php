<?php
declare(strict_types=1);
namespace Bss\FormSample\Controller\Adminhtml\Index;

use Bss\FormSample\Model\ResourceModel\Article\Collection;

class MassDelete extends AbstractMassAction
{
    /**
     * Mass delete articles.
     */
    protected function massAction()
    {
        /**
         * @var Collection
         */
        $collection = $this->getCollection();
        $collection->delete();
        $this->messageManager->addSuccessMessage(__('You deleted the articles'));
    }
}
