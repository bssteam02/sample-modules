<?php
declare(strict_types=1);
namespace Bss\FormSample\Controller\Adminhtml\Index;

use Bss\FormSample\Controller\Adminhtml\AbstractArticle;
use Magento\Framework\App\Action\HttpPostActionInterface;
use Magento\Framework\Controller\ResultInterface;

class Delete extends AbstractArticle implements HttpPostActionInterface
{
    /**
     * @return ResultInterface
     */
    public function execute()
    {
        try {
            $id = $this->getRequest()->getParam('article_id');
            $this->articleRepository->delete($id);
            $this->messageManager->addSuccessMessage(__("You deleted the article"));
        } catch (\Exception $e) {
            $this->messageManager->addErrorMessage($e->getMessage());
        }
        return $this->resultFactory->create($this->resultFactory::TYPE_REDIRECT)->setPath('*/*');
    }
}
