<?php
declare(strict_types=1);
namespace Bss\FormSample\Api\Data;

interface ArticleInterface
{
    const ARTICLE_ID = 'article_id';
    const IS_ACTIVE = 'is_active';
    const TITLE = 'title';
    const TOPIC = 'topic';
    const CONTENT = 'content';
    const PREVIEW_IMG = 'preview_img';

    /**
     * Get Article ID
     *
     * @return int|null
     */
    public function getArticleId();

    /**
     * Set Article ID
     *
     * @param int $id
     * @return ArticleInterface
     */
    public function setArticleId($id);

    /**
     * Get status
     *
     * @return int|null
     */
    public function getIsActive();

    /**
     * Set status
     *
     * @param int $status
     * @return ArticleInterface
     */
    public function setIsActive($status);

    /**
     * Get Title
     *
     * @return string
     */
    public function getTitle();

    /**
     * Set Title
     *
     * @param string $title
     * @return ArticleInterface
     */
    public function setTitle($title);

    /**
     * Get Preview Image
     *
     * @return string
     */
    public function getPreviewImg();

    /**
     * Set Preview Image
     *
     * @param string $image
     * @return ArticleInterface
     */
    public function setPreviewImg($image);

    /**
     * Get Topic
     *
     * @return string
     */
    public function getTopic();

    /**
     * Set Longitude
     *
     * @param string $topic
     * @return ArticleInterface
     */
    public function setTopic($topic);

    /**
     * Get Latitude
     *
     * @return string
     */
    public function getContent();

    /**
     * Set Latitude
     *
     * @param string $content
     * @return ArticleInterface
     */
    public function setContent($content);
}
