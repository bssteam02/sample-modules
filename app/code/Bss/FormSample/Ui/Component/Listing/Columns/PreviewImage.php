<?php
declare(strict_types=1);
namespace Bss\FormSample\Ui\Component\Listing\Columns;

use Bss\FormSample\Model\Article;
use Magento\Framework\View\Asset\Repository as AssetRepository;
use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\View\Element\UiComponentFactory;

class PreviewImage extends \Magento\Ui\Component\Listing\Columns\Column
{
    const NAME = 'preview_img';

    /**
     * @var \Magento\Framework\UrlInterface
     */
    protected $urlBuilder;

    /**
     * @var Article\PreviewImage
     */
    protected $articlePreviewImage;

    /**
     * @var AssetRepository
     */
    protected $assetRepository;

    /**
     * @param ContextInterface $context
     * @param UiComponentFactory $uiComponentFactory
     * @param AssetRepository $assetRepository
     * @param \Magento\Framework\UrlInterface $urlBuilder
     * @param Article\PreviewImage $articlePreviewImage
     * @param array $components
     * @param array $data
     */
    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        AssetRepository $assetRepository,
        \Magento\Framework\UrlInterface $urlBuilder,
        Article\PreviewImage $articlePreviewImage,
        array $components = [],
        array $data = []
    ) {
        $this->assetRepository = $assetRepository;
        $this->articlePreviewImage = $articlePreviewImage;
        $this->urlBuilder = $urlBuilder;
        parent::__construct($context, $uiComponentFactory, $components, $data);
    }

    /**
     * Prepare Data Source
     *
     * @param array $dataSource
     * @return array
     */
    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            $fieldName = $this->getData('name');
            foreach ($dataSource['data']['items'] as & $item) {
                $imageUrl = $this->articlePreviewImage->getImageUrl($item[Article::PREVIEW_IMG]);
                if (!$imageUrl) {
                    $imageUrl = $this->getDefaultImage();
                }
                $item[$fieldName . '_src'] = $imageUrl;
                $item[$fieldName . '_orig_src'] = $imageUrl;
                $item[$fieldName . '_alt'] = $item[Article::TITLE];
                $item[$fieldName . '_link'] = $this->urlBuilder->getUrl(
                    'article/index/edit',
                    ['article' => $item[Article::ARTICLE_ID]]
                );
            }
        }

        return $dataSource;
    }

    /**
     * @return string
     */
    private function getDefaultImage()
    {
        return $this->assetRepository->getUrl("Bss_FormSample::image/no-image.jpeg");
    }
}
